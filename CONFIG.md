# Configuration

## Tool description

In `src/config/tools`, you have all the tools made available in the application, each one being described as a JSON file. The structure of a tool JSON file is the following:

```js
{
	"name": "Cool tool",
	"catchphrase": "Making changes easily",
	"logo": "https://cool.tool/logo.jpg",
	"picture": "https://cool.tool/picture.jpg",
	"description": "Cool tool is great for editing OSM data on a given thematic. You can watch and edit tags. It is awesome for this kind of tasks.",
	"start_url": "https://cool.tool/editor",
	"doc_url": "https://cool.tool/documentation",
	"settings": {
		"duration": "seconds",
		"where": "outside",
		"difficulty": "easy"
	}
}
```

Each field is used to describe a contribution tool. They follow these rules, and all are mandatory (unless explicitely said):
* `name`: name of the tool, as users see it.
* `catchphrase`: short summary of what tool can do, in less than 50 characters.
* `logo` _(optional)_: URL to the tool logo (PNG, JPG).
* `picture`: URL to a picture of the tool in action (PNG, JPG).
* `description`: A text describing what the tool does, and what is its interest. At least 30 characters, and might be not too long.
* `start_url`: URL to the page where you can start using the tool. For smartphone applications, page where you have links to stores.
* `doc_url`: URL to the main page of documentation. If no dedicated documentation, can be the tool page on OSM wiki.
* `settings`
 * `duration`: minimal required time to contribute on a single task. Values: `seconds`, `minutes`, `hours`, `days`.
 * `where`: location prefered to use the tool. Values: `outside` (need ground survey), `inside` (remote work, desktop tools), `everywhere` (smartphone & desktop tools).
 * `difficulty`: level of knowledge needed by user. Values: `easy` (anyone), `intermediate` (bit of OSM knowledge needed), `difficult` (lot of OSM knowledge needed), `complex` (for OSM gurus).

Validity of these files can be checked by running `npm run build:tooljson` at the root of the project.
