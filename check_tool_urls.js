/**
 * Check if every URL is working live in tool JSON file.
 */

const fs = require('fs');
const request = require('request');
const tools = require('./src/www/tools.js');

const testResult = (toolId, urlType, err, res) => {
	if(err) {
		throw new Error("[ERROR] "+toolId+" ("+urlType+") => "+err);
	}
	else if(!res) {
		throw new Error("[ERROR] "+toolId+" ("+urlType+") => Empty result");
	}
	else if(res.statusCode !== 200) {
		console.log(err);
// 		console.log(res);
		throw new Error("[ERROR] "+toolId+" ("+urlType+") => Response code != 200 ("+res.statusCode+")");
	}
	else {
		console.log("[OK] "+toolId+" ("+urlType+")");
	}
};

const options = {
	headers: {},
	timeout: 15000
};

for(const toolId in tools) {
	if(tools[toolId].logo.length > 0) {
		request.get(tools[toolId].logo, options, (err, res, body) => {
			testResult(toolId, "logo", err, res);
		});
	}

	request.get(tools[toolId].picture, options, (err, res, body) => {
		testResult(toolId, "picture", err, res);
	});

	request.get(tools[toolId].start_url, options, (err, res, body) => {
		testResult(toolId, "start_url", err, res);
	});

	request.get(tools[toolId].doc_url, options, (err, res, body) => {
		testResult(toolId, "doc_url", err, res);
	});
}
