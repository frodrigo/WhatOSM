import React, {Component} from 'react';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { Step, Stepper, StepButton, StepContent } from 'material-ui/Stepper';
import DifficultySetting from './DifficultySetting';
import DurationSetting from './DurationSetting';
import WhereSetting from './WhereSetting';

const NB_STEPS = 3;

const styles = {
	setting: {
		margin: '15px 0 0 15px'
	}
};


/**
 * Settings is the component asking user for its task preference.
 */
class Settings extends Component {
	constructor(props, context) {
		super(props, context);
		
		this.state = {
			stepIndex: 0,
			firstRun: true,
			settings: {}
		};
	}
	
	/**
	 * Handler when setting step is changed
	 * @param {int} step The new step ID
	 */
	handleStepChange(step) {
		if(this.state.firstRun) {
			if(step == this.state.stepIndex + 1) {
				this.setState({
					stepIndex: step,
					firstRun: step !== NB_STEPS
				});
			}
		}
		else {
			this.setState({
				stepIndex: step
			});
		}
	}
	
	/**
	 * Handler when a setting value has changed.
	 * @param {string} setting The setting name
	 * @param {string} value The setting value
	 */
	handleSettingChange(setting, value) {
		let newStepIndex = this.state.firstRun ? this.state.stepIndex + 1 : this.state.stepIndex;
		let newSetting = Object.assign({}, this.state.settings);
		newSetting[setting] = value;
		
		this.setState({
			settings: newSetting
		});
		
		if(this.props.onChange) {
			this.props.onChange(setting, value);
		}
		
		setTimeout(() => {
			this.handleStepChange(newStepIndex);
			
			if(this.props.onDone && newStepIndex >= NB_STEPS) {
				this.props.onDone();
			}
		}, this.state.firstRun ? 400 : 0);
	}
	
	render() {
		return (
			<div>
				<Stepper linear={this.state.firstRun} orientation="vertical" activeStep={this.state.stepIndex}>
					<Step completed={this.state.stepIndex > 0}>
						<StepButton onClick={() => this.handleStepChange(0)}>
							{I18n.t("Difficulty level ?")}
						</StepButton>
						<StepContent>
							<DifficultySetting style={styles.setting} onChange={v => this.handleSettingChange("difficulty", v)} defaultValue={this.state.settings["difficulty"]} />
						</StepContent>
					</Step>
					<Step completed={this.state.stepIndex > 1}>
						<StepButton onClick={() => this.handleStepChange(1)}>
							{I18n.t("Duration ?")}
						</StepButton>
						<StepContent>
							<DurationSetting style={styles.setting} onChange={v => this.handleSettingChange("duration", v)} defaultValue={this.state.settings["duration"]} />
						</StepContent>
					</Step>
					<Step completed={this.state.stepIndex > 2}>
						<StepButton onClick={() => this.handleStepChange(2)}>
							{I18n.t("Where ?")}
						</StepButton>
						<StepContent>
							<WhereSetting style={styles.setting} onChange={v => this.handleSettingChange("where", v)} defaultValue={this.state.settings["where"]} />
						</StepContent>
					</Step>
				</Stepper>
			</div>
		);
	}
}

export default Settings;
